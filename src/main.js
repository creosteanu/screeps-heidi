require('ExtendedTower');
require('ExtendedSpawn');
require('ExtendedRoom');
require('ExtendedCreep');
require('MyConstants');

let harvester = require('role.harvester');
let distributor = require('role.distributor');
let upgrader = require('role.upgrader');
let builder = require('role.builder');
let wallBuilder = require('role.wallBuilder');
let settler = require('role.settler');

let roles = {
    harvester,
    distributor,
    upgrader,
    builder,
    wallBuilder,
    settler
};

module.exports.loop = function () {
    // Deleting memory of dead creeps
    for (let name in Memory.creeps) {
        if (!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing dead creep memory: ' + name);
        }
    }

    for (let name in Game.rooms) {
        let room = Game.rooms[name];

        room.run();
    }

    for (let name in Game.spawns) {
        let spawn = Game.spawns[name];

        spawn.createCreeps();
    }

    // Running the creeps
    for (let name in Game.creeps) {
        let creep = Game.creeps[name];

        if (roles[creep.memory.role]) {
            let instanceOfClass = new roles[creep.memory.role](creep);
            instanceOfClass.run();
        }
    }
};