Creep.getBodyCost = function (body) {
    let bodyCost = 0;

    for (let part of body) {
        bodyCost += BODYPART_COST[part];
    }

    return bodyCost;
};

Creep.prototype.harvestFromSource = function () {
    let source = this.pos.findClosestByRange(FIND_SOURCES, src => src.energy > 0);

    if (this.pos.isNearTo(source)) {
        this.harvest(source);
    } else {
        this.moveTo(source);
    }
};

Creep.prototype.withdrawEnergyFromStorage = function () {
    let storage = this.room.storage;
    let amount = this.carryCapacity - this.carry.energy;

    if (this.pos.isNearTo(storage)) {
        this.withdraw(storage, RESOURCE_ENERGY, amount);
    } else {
        this.moveTo(storage);
    }
};

Creep.prototype.getEnergy = function () {
    let storage = this.room.storage;

    if (storage && storage.store[RESOURCE_ENERGY] > 5000) {
        this.withdrawEnergyFromStorage();
    } else {
        this.harvestFromSource();
    }
};