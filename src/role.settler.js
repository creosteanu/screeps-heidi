class Settler {

    constructor(creep) {
        this.creep = creep;
        this.targetController = Game.rooms[this.memory.targetRoomId].controller;
    }

    static getBody() {
        return [MOVE, CLAIM];
    }

    run() {
        if (this.creep.pos.inRangeTo(this.targetController, 3)) {
            this.creep.claimController(this.targetController);
        } else {
            this.creep.moveTo(this.targetController);
        }
    }
}

module.exports = Settler;