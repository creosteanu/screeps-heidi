/* This class models creeps that harvest energy and transfer it to the storage.
 */

class Harvester {

    constructor(creep) {
        this.creep = creep;
        this.memory = creep.memory;
    }

    /* Formula is: [(3*d + 17) * 5] / (25 * 2)
     * Where: 3*d + 17 is a harvesting cycle (traveling time + harvesting time + transfer time - adjustment)
     *        5 is energy/tick creep has to harvest to fully exploit source
     *        25 is the harvesting time
     *        2 is energy/tick one work part gets
     */
    static getBody(distanceToSource) {
        let body = [WORK, CARRY, MOVE];
        let sets = Math.ceil((3 * distanceToSource + 17) / 10);

        for (let i = 1; i < sets; i++) {
            body.push(WORK, CARRY, MOVE);
        }

        return body;
    }

    checkDecayingRoad() {
        let structureHere = this.creep.pos.lookFor(LOOK_STRUCTURES);
        let roadHere = _.find(structureHere, structure => structure.structureType === STRUCTURE_ROAD);

        if (roadHere && roadHere.hits < roadHere.hitsMax) {
            this.creep.repair(roadHere);
        }
    }

    transferEnergy() {
        let storage = this.creep.room.storage;

        if (this.creep.pos.isNearTo(storage)) {
            this.checkDecayingRoad();
            this.creep.transfer(storage, RESOURCE_ENERGY);
        } else {
            this.creep.moveTo(storage);
        }
    }

    // Do not replace with harvestFromSource() from ExtendedCreep because this one uses sourceId from the creep's memory
    harvest() {
        let source = Game.getObjectById(this.memory.sourceId);

        if (this.creep.pos.isNearTo(source)) {
            this.creep.harvest(source);
        } else {
            this.creep.moveTo(source);
        }
    }

    setObjective() {
        if (this.memory.harvesting && this.creep.carry.energy === this.creep.carryCapacity) {
            this.memory.harvesting = false;
        }

        if (!this.memory.harvesting && this.creep.carry.energy === 0) {
            this.memory.harvesting = true;
        }
    }

    run() {
        this.setObjective();

        if (this.memory.harvesting) {
            this.harvest();
        } else {
            this.checkDecayingRoad();
            this.transferEnergy();
        }
    }
}

module.exports = Harvester;