let Harvester = require('role.harvester');
let Distributor = require('role.distributor');
let Upgrader = require('role.upgrader');
let Builder = require('role.builder');
let WallBuilder = require('role.wallBuilder');

/* WALLBUILDER */
StructureSpawn.prototype.spawnWallBuilder = function () {
    this.room.memory.willSpawn = true;
    let newName = this.createCreep(WallBuilder.getBody(), undefined, {role: ROLE_WALL_BUILDER});
    console.log('Spawning new wallBuilder: ' + newName);
};

StructureSpawn.prototype.checkNeedWallBuilder = function () {
    if (!this.room.memory.willSpawn) {
        let wallBuilders = _.filter(Memory.creeps, creep => creep.role === ROLE_WALL_BUILDER);
        let maxWallBuilders = this.room.memory.maxCreeps[ROLE_WALL_BUILDER];

        if ((wallBuilders.length === maxWallBuilders && this.room.hasOldCreep(ROLE_WALL_BUILDER)) || wallBuilders.length < maxWallBuilders) {
            this.spawnWallBuilder();
        }
    }
};

/* BUILDER */
StructureSpawn.prototype.spawnBuilder = function () {
    this.room.memory.willSpawn = true;
    let newName = this.createCreep(Builder.getBody(), undefined, {role: ROLE_BUILDER});
    console.log('Spawning new builder: ' + newName);
};

StructureSpawn.prototype.checkNeedBuilder = function () {
    if (!this.room.memory.willSpawn) {
        let builders = _.filter(Memory.creeps, creep => creep.role === ROLE_BUILDER);
        let maxBuilders = this.room.memory.maxCreeps[ROLE_BUILDER];
        let hasConstructionSites = this.room.find(FIND_MY_CONSTRUCTION_SITES).length;

        if (hasConstructionSites) {
            if ((builders.length === maxBuilders && this.room.hasOldCreep(ROLE_BUILDER) || builders.length < maxBuilders)) {
                this.spawnBuilder();
            }
        }
    }
};

/* UPGRADER */
StructureSpawn.prototype.spawnUpgrader = function () {
    this.room.memory.willSpawn = true;
    let newName = this.createCreep(Upgrader.getBody(), undefined, {role: ROLE_UPGRADER});
    console.log('Spawning new upgrader: ' + newName);
};

StructureSpawn.prototype.checkNeedUpgrader = function () {
    if (!this.room.memory.willSpawn) {
        let upgraders = _.filter(Memory.creeps, creep => creep.role === ROLE_UPGRADER);
        let maxUpgraders = this.room.memory.maxCreeps[ROLE_UPGRADER];

        if ((upgraders.length === maxUpgraders && this.room.hasOldCreep(ROLE_UPGRADER)) || upgraders.length < maxUpgraders) {
            this.spawnUpgrader();
        }
    }
};

/* HARVESTER */
StructureSpawn.prototype.setHarvesterMemory = function (harvesters, maxHarvesters) {
    let sources = this.room.find(FIND_SOURCES);
    let assignedMemory = {role: ROLE_HARVESTER, sourceId: sources[0].id, distanceToSource: sources[0].pos.findPathTo(this.room.storage).length};
    let distributedHarvesters = _.countBy(harvesters, creep => creep.sourceId);    // Holds source ids as keys and number of harvesters as values

    for (let id in distributedHarvesters) {
        if (distributedHarvesters[id] < maxHarvesters / sources.length) {
            assignedMemory.sourceId = id;
            assignedMemory.distanceToSource = Game.getObjectById(id).pos.findPathTo(this.room.storage).length;      // path from source to storage
            break;
        }
    }

    return assignedMemory;
};

StructureSpawn.prototype.spawnHarvester = function (harvesters, maxHarvesters) {
    this.room.memory.willSpawn = true;
    let assignedMemory = this.setHarvesterMemory(harvesters, maxHarvesters);
    let newName = this.createCreep(Harvester.getBody(assignedMemory.distanceToSource), undefined, assignedMemory);
    console.log('Spawning new harvester: ' + newName);
};

StructureSpawn.prototype.spawnHarvesterWithMemory = function (knowledge) {
    this.room.memory.willSpawn = true;
    let newName = this.createCreep(Harvester.getBody(knowledge.distanceToSource), undefined, knowledge);
    console.log('Spawning new harvester: ' + newName);
};

StructureSpawn.prototype.checkNeedHarvester = function () {
    if (!this.room.memory.willSpawn) {
        let harvesters = _.filter(Memory.creeps, creep => creep.role === ROLE_HARVESTER);
        let maxHarvesters = this.room.memory.maxCreeps[ROLE_HARVESTER];
        let harvesterKnowledge = this.room.getOldHarvesterKnowledge();

        if (harvesters.length === maxHarvesters && harvesterKnowledge) {
            this.spawnHarvesterWithMemory(harvesterKnowledge);
        } else if (harvesters.length < maxHarvesters) {
            this.spawnHarvester(harvesters, maxHarvesters);
        }
    }
};

/* DISTRIBUTOR */
StructureSpawn.prototype.spawnDistributor = function () {
    this.room.memory.willSpawn = true;
    let newName = this.createCreep(Distributor.getBody(this.room), undefined, {role: ROLE_DISTRIBUTOR});
    console.log('Spawning new distributor: ' + newName);
};

StructureSpawn.prototype.checkNeedDistributor = function () {
    if (!this.room.memory.willSpawn) {
        let distributors = _.filter(Memory.creeps, creep => creep.role === ROLE_DISTRIBUTOR);
        let maxDistributors = this.room.memory.maxCreeps[ROLE_DISTRIBUTOR];

        if ((distributors.length === maxDistributors && this.room.hasOldCreep(ROLE_DISTRIBUTOR)) || distributors.length < maxDistributors) {
            this.spawnDistributor();
        }
    }
};

StructureSpawn.prototype.createCreeps = function () {
    if (!this.spawning) {
        this.checkNeedDistributor();

        if (this.room.energyAvailable === this.room.energyCapacityAvailable) {
            this.checkNeedHarvester();
            this.checkNeedUpgrader();
            this.checkNeedBuilder();
            this.checkNeedWallBuilder();
        }
    }
};