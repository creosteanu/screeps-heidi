StructureTower.prototype.attackNearestEnemy = function () {
    let notMyCreeps = this.room.find(FIND_HOSTILE_CREEPS);
    let hostileCreeps = _.filter(notMyCreeps, creep => !_.includes(Memory.allies, creep.owner.username));
    let closestHostile = this.pos.findClosestByRange(hostileCreeps);

    if (closestHostile) {
        this.attack(closestHostile);
    }
};

