Room.prototype.getOldHarvesterKnowledge = function () {
    let harvesters = _.filter(this.find(FIND_MY_CREEPS), creep => creep.memory.role === ROLE_HARVESTER);
    let knowledge;

    for (let harvester of harvesters) {
        let spawningTime = 3 * harvester.body.length;

        if (harvester.ticksToLive < spawningTime) {
            knowledge = harvester.memory;
            break;
        }
    }

    return knowledge;
};

Room.prototype.hasOldCreep = function (roleName) {
    let creeps = _.filter(this.find(FIND_MY_CREEPS), creep => creep.memory.role === roleName);
    let has = false;

    for (let creep of creeps) {
        let spawningTime = 3 * creep.body.length;

        if (creep.ticksToLive < spawningTime) {
            has = true;
            break;
        }
    }

    return has;
};

Room.prototype.printCreeps = function () {
    let creeps = this.find(FIND_MY_CREEPS);

    let harvesters = _.filter(creeps, creep => creep.memory.role === ROLE_HARVESTER);
    let distributors = _.filter(creeps, creep => creep.memory.role === ROLE_DISTRIBUTOR);
    let upgraders = _.filter(creeps, creep => creep.memory.role === ROLE_UPGRADER);
    let builders = _.filter(creeps, creep => creep.memory.role === ROLE_BUILDER);
    let wallBuilders = _.filter(creeps, creep => creep.memory.role === ROLE_WALL_BUILDER);

    console.log(this.name + " harvesters: " + harvesters.length);
    console.log(this.name + " distributors: " + distributors.length);
    console.log(this.name + " upgraders: " + upgraders.length);
    console.log(this.name + " builders: " + builders.length);
    console.log(this.name + " wall builders: " + wallBuilders.length);
    console.log();
};

Room.prototype.protectRoom = function () {
    let structures = this.find(FIND_MY_STRUCTURES);
    let towers = _.filter(structures, (structure) => structure.structureType === STRUCTURE_TOWER);

    for (let i = 0; i < towers.length; i++) {
        towers[i].attackNearestEnemy();
    }
};

/* This method will be called at the beginning of each tick. */
Room.prototype.run = function () {
    this.protectRoom();
    this.printCreeps();
    this.memory.willSpawn = false;
};