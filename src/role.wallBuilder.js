/* This class models creeps that maintain (build) walls and ramparts.
 * They harvest from sources and find the weakest wall/rampart to maintain.
 */

class WallBuilder {

    constructor(creep) {
        this.creep = creep;
        this.memory = creep.memory;
        this.storage = creep.room.storage;
    }

    static getBody() {
        let body = [WORK, CARRY, MOVE];
        let sets = 5;

        for (let i = 1; i < sets; i++) {
            body.push(WORK, CARRY, MOVE);
        }

        return body;
    }

    buildWall() {
        let weakWall = Game.getObjectById(this.memory.target);

        if (weakWall) {
            if (this.creep.pos.inRangeTo(weakWall, 3)) {
                this.creep.repair(weakWall);
            } else {
                this.creep.moveTo(weakWall);
            }
        } else {
            delete this.memory.building;
        }
    }

    setTarget() {
        let structures = this.creep.room.find(FIND_STRUCTURES);
        let walls = _.filter(structures, structure => structure.structureType === STRUCTURE_WALL || structure.structureType === STRUCTURE_RAMPART);
        let sortedWalls = _.sortBy(walls, wall => wall.hits);

        this.memory.target = sortedWalls[0].id;
    }

    setObjective() {
        if (this.memory.building && this.creep.carry.energy === 0) {
            this.memory.building = false;
        }

        if (!this.memory.building && this.creep.carry.energy === this.creep.carryCapacity) {
            this.memory.building = true;
            this.setTarget();
        }
    }

    run() {
        this.setObjective();

        if (this.memory.building) {
            this.buildWall();
        } else {
            this.creep.getEnergy();
        }
    }
}

module.exports = WallBuilder;