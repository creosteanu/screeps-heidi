/* This class models creeps that build on construction sites.
 * They harvest energy from sources and find the closest construction site.
 */

class Builder {

    constructor(creep) {
        this.creep = creep;
        this.memory = creep.memory;
        this.storage = creep.room.storage;
    }

    static getBody() {
        let body = [WORK, CARRY, MOVE];
        let sets = 4;

        for (let i = 1; i < sets; i++) {
            body.push(WORK, CARRY, MOVE);
        }

        return body;
    }

    build() {
        let constructionSite = this.creep.pos.findClosestByRange(FIND_MY_CONSTRUCTION_SITES);

        if (constructionSite) {
            if (this.creep.pos.inRangeTo(constructionSite, 3)) {
                this.creep.build(constructionSite);
            } else {
                this.creep.moveTo(constructionSite);
            }
        } else {
            this.memory.role = ROLE_WALL_BUILDER;
        }
    }

    setObjective() {
        if (this.memory.building && this.creep.carry.energy === 0) {
            this.memory.building = false;
        }

        if (!this.memory.building && this.creep.carry.energy === this.creep.carryCapacity) {
            this.memory.building = true;
        }
    }

    run() {
        this.setObjective();

        if (this.memory.building) {
            this.build();
        } else {
            this.creep.getEnergy();
        }
    }
}

module.exports = Builder;