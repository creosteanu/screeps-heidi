/* This class models creeps that withdraw energy from the storage and distribute it to the spawn, extensions and towers.
 */

class Distributor {

    constructor(creep) {
        this.creep = creep;
        this.storage = creep.room.storage;
    }

    static getBody(room) {
        // Between 1 and 6 sets of body parts
        let body = [CARRY, MOVE];
        let maxSets = 6;
        let bodyCost = Creep.getBodyCost(body);
        let nBodies = Math.floor(room.energyAvailable / bodyCost);

        for (let i = 1; i < maxSets && i < nBodies; i++) {
            body.push(CARRY, MOVE);
        }

        return body;
    }

    setTarget() {
        return this.creep.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType === STRUCTURE_EXTENSION ||
                        structure.structureType === STRUCTURE_SPAWN ||
                        structure.structureType === STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
            }
        });
    }

    transferEnergy() {
        let target = this.setTarget();

        if (target) {
            if (this.creep.pos.isNearTo(target)) {
                this.creep.transfer(target, RESOURCE_ENERGY);
            } else {
                this.creep.moveTo(target);
            }
        } else {
            this.creep.moveTo(this.storage);     // Ready to withdraw energy from storage when needed
        }
    }

    run() {
        if (this.creep.carry.energy >= 50) {
            this.transferEnergy();
        } else {
            this.creep.withdrawEnergyFromStorage();
        }
    }
}

module.exports = Distributor;