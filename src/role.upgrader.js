/* This class models creeps that upgrade the room controller.
 * They prefer taking energy from the storage, but can also harvest from sources if necessary.
 */

class Upgrader {

    constructor(creep) {
        this.creep = creep;
        this.memory = creep.memory;
        this.storage = creep.room.storage;
    }

    static getBody() {
        let body = [WORK, CARRY];
        let sets = 5;

        for (let i = 1; i < sets; i++) {
            body.push(WORK, CARRY);
        }
        body.push(MOVE);

        return body;
    }

    upgradeController() {
        let controller = this.creep.room.controller;

        if (this.creep.pos.inRangeTo(controller, 3)) {
            this.creep.upgradeController(controller);
        } else {
            this.creep.moveTo(controller);
        }
    }

    setObjective() {
        if (this.memory.upgrading && this.creep.carry.energy === 0) {
            this.memory.upgrading = false;
        }

        if (!this.memory.upgrading && this.creep.carry.energy === this.creep.carryCapacity) {
            this.memory.upgrading = true;
        }
    }

    run() {
        this.setObjective();

        if (this.memory.upgrading) {
            this.upgradeController();
        } else {
            this.creep.getEnergy();
        }
    }
}

module.exports = Upgrader;